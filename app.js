const { Socket } = require('dgram');
const { TIMEOUT } = require('dns');
const net = require('net');
const { resolve } = require('path');
const { data } = require('./logger');
const logger = require('./logger')





async function start () {

    //Revisamos si los parametros son correctos

    const CENTRAL_IP = process.argv[2];
    const COMMAND = process.argv[3]
    const TIMEOUT = parseInt(process.argv[4]) || 5000

    
    //Revisamos que la ip sea valida
    let regexp = new RegExp(/(^\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b$)/);
    if (!regexp.test(CENTRAL_IP)) {
        logger.info('ERROR en la ip')
        process.exit(1)
    }

    if (isNaN(TIMEOUT)) {
        logger.info('El timeout debe ser numerico')
        process.exit(1)
    }

    logger.info('Enviando comando');
    await sendCommand(CENTRAL_IP, COMMAND, TIMEOUT)
    .then(response => {
        logger.info('Datos recibidos: ' + response);
        logger.info('Proceso terminado')
    })
    .catch(e => {logger.error(e)})

    process.exit();
}

function sendCommand (ip, command, timeout) {
    return new Promise((resolve, reject) => {
        
        var socket = new net.Socket();
        var result = ''

        socket.connect(23, ip, () => {
            logger.info('Conectado a la central: ' + ip);
            logger.info('Enviado comando: ' + command+'\r\n' );

            socket.write(command+'\r\n');
        })

        socket.setTimeout(timeout);
        socket.on('timeout', () => {
            logger.info('Timeout!')
            socket.end();
            resolve(result);
        })

        socket.on('data', (data) => {
            logger.info('--> ' + data.toString())
            result += data.toString()
        })

        socket.on('error', (err) => {
            logger.error('Error: ' + err)
            reject(err);
        })

    })
}

//Init
start();
