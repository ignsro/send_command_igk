# Send_Command

App para enviar comando a central vía telnet.

## Instalacion

Usa [npm](https://www.npmjs.com) para instalar.

```bash
npm install
```

## Uso

Para usar la aplicación basta con iniciar el proyecto con node.

```bash
node app.js [ip] [comando] [timeout]
```
El timeout (ms) es opcional, su valor por defecto en 5 segundos.

## Ejemplo

```bash
node app.js 192.168.0.100 DUMP; 10000

node app.js 192.168.0.100 QUERY:123456;
```