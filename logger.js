  const { createLogger, format, transports } = require('winston');

module.exports = createLogger({
    level: 'debug',
    format: format.combine(format.simple()),
    transports: [
        new transports.File({
            maxsize: 5120000,
            maxFiles: 5,
            filename: 'history.log'
        }),
        new transports.Console({
            level: 'debug',
        }),
        new transports.File({ filename: 'history.log', level: 'error' })
    ] 
})